import java.util.*;

/**
 * Die Klasse Mitglied ist eine Subklasse von Person. Sie speichert Felder und
 * Methoden, die nur beim Einloggen in die App als Mitglied verwendet werden.
 *
 * @author Clemens Kerber
 * @version 1.0 (15.05.2021)
 */

public class Mitglied extends Person {
    private float hoehe;
    private float startGewicht;
    private String gewaehlesPacket;
    public HashMap<String, Bewertung> bewertung;

    /**
     * Constructor für die Klasse Mitglieder
     *
     * @param name Namen des Mitglieds
     * @param email E-Mail des Mitglieds
     * @param address Adresse des Mitglieds
     * @param geschlecht Geschlecht des Mitglieds
     * @param hoehe Größe des Mitglieds
     * @param startGewicht Startgewicht des Mitglieds
     * @param gewaehltesPacket Packetwahl des Mitglieds
     */
    public Mitglied(String name, String email, String address, String geschlecht, float hoehe, float startGewicht, String gewaehltesPacket) {
        super(name, email, address, geschlecht);
        setHoehe(hoehe);
        setStartGewicht(startGewicht);
        setGewaehltesPacket(gewaehltesPacket);
        //bewertung auf eine neue HashMap setzen
        bewertung = new HashMap<String, Bewertung>();
    }

    /**
     * Methode, die die letzte Bewertung nach Datum aus dem Set der Bewertungensfeldern eines Mitglieds
     * in eine neue HashMap zurückliefert.
     *
     * @return Bewertungsobjekt, das zuletzt am Mitglied durchgeführt wurde
     */
    public Bewertung letzteBewertung() {
        //Verwenden von SortedSet, um die Bewertungsdaten in chronologischer Reihenfolge zu erhalten.
        SortedSet<String> sortierteBewertung = sortierteBewertungDatum();
        Bewertung ergebnis = null;
        if(!sortierteBewertung.isEmpty()){
            // Das letzte Element, das dem Set sortierteBewertung hinzugefügt wurde, ist das aktuellste.
            String letztesDatum = sortierteBewertung.last();
            ergebnis = bewertung.get(letztesDatum);
        }
        return ergebnis;
    }

    /**
     * Methode, die die Bewertungen aus den bewertung HashMap übernimmt
     * und sortiert sie nach dem neuesten zuerst als SortedSet.
     *
     * @return SortedSet<String>, das die Bewertungsdaten enthält, sortiert nach dem neuesten zuerst.
     */
    public SortedSet<String> sortierteBewertungDatum(){
        //TreeSet verwendet, um die Bewertungsdaten in chronologische Reihenfolge zu bringen
        SortedSet<String> bewertungsDatum = new TreeSet<>();
        Iterator it = bewertung.entrySet().iterator();
        while (it.hasNext()){
            Map.Entry bewertung = (Map.Entry)it.next();
            // Iterieren über das Set und hinzufügen des Datums BewertungsDatum.
            String date = (String)bewertung.getKey();
            bewertungsDatum.add(date);
        }
        return bewertungsDatum;
    }

    /**
     * Leere Methode, die in PremiumMember- und StudentMember-Klassen überschriebt.
     *
     * @param gewaehltesPacket Nimmt das vom Mitglied gewählte Paket auf.
     */
    public void gewaehltesPacket(String gewaehltesPacket){
    };

    /**
     * Methode zum Abrufen des zuletzt für ein Mitglied aufgezeichneten Gewichts.
     *
     * @return float Wert des letzen Gewichts.
     */
    public float getletztesGewicht(){
        if(bewertung.isEmpty()){
            return startGewicht;
        }else{
            return letzteBewertung().getGewicht();
        }
    }

    /**
     * Methode zur Anzeige von Bewertungen sortiert nach Gewicht - vom niedrigsten zum höchsten.
     *
     * @return Es wird ein String zurückgegeben, der die Bewertungsgewichtungen in der Reihenfolge vom niedrigsten zum höchsten anzeigt.
     */
    public String getBewertungMitGewicht(){
        String ergebnis = "";
        SortedSet<String> bewertungMitGewicht = new TreeSet<>();
        Iterator it = bewertung.entrySet().iterator();
        if(!bewertung.isEmpty()){
            while(it.hasNext()){
                Map.Entry bewertung = (Map.Entry)it.next();
                Bewertung geordneteBewertung = (Bewertung) bewertung.getValue();
                String datum = (String) bewertung.getKey();
                float gewicht = geordneteBewertung.getGewicht();
                bewertungMitGewicht.add(gewicht+ "kg - "+ datum);
            }
            ergebnis += "Deine Bewertung sortiert nach Gewicht: \n";
            for(String bewertung : bewertungMitGewicht){
                ergebnis += bewertung +"\n";
            }
        }else{
            ergebnis = "Keine Berwertung vorhanden";
            System.out.println("");
        }
        return ergebnis;
    }

    /**
     * Methode zur Anzeige von Bewertungen sortiert nach Taille - niedrigste bis höchste.
     *
     * @return Es wird ein String zurückgegeben, der die Bewerteten-Taillen in der Reihenfolge vom niedrigsten zum höchsten anzeigt.
     */
    public String getBewertungMitTaille(){
        String ergebnis = "";
        SortedSet<String> bewertungMitTaille = new TreeSet<>();
        Iterator it = bewertung.entrySet().iterator();
        if(!bewertung.isEmpty()){
            while (it.hasNext()){
                Map.Entry bewertung = (Map.Entry)it.next();
                Bewertung sortierteBewertung = (Bewertung) bewertung.getValue();
                String datum = (String) bewertung.getKey();
                float taille  = sortierteBewertung.getTaille();
                bewertungMitTaille.add(taille+ "cm - " + datum);
            }
            ergebnis += "Deine Bewertung sortiert nach Taillenmessung: \n";
            for (String bewertung : bewertungMitTaille){
                ergebnis += bewertung + "\n";
            }
        }else{
            ergebnis = "Keine Bewertung vorhanden";
            System.out.println("");
        }
        return ergebnis;
    }

    /**
     * Getter für das Startgewicht.
     *
     * @return startGewicht als float Wert.
     */
    public float getStartGewicht() {
        return startGewicht;
    }

    /**
     * Setter für das Startgewicht. Der Wert kann nicht kleiner als 35.0 oder höher 250.0 sein.
     *
     * @param startGewicht  nimmt den Startgewichtswert an
     */
    public void setStartGewicht(Float startGewicht) {
        try {
            if ((startGewicht < 35.0) || (startGewicht > 250.0)) {
                    System.out.println("Startgewicht muss zwischen 35kg und 250kg liegen!");
                } else {
                    this.startGewicht = startGewicht;
                }
            }catch(Exception e){
                System.out.println("Undefinierter Input:" + e);
            }
    }

    /**
     * Getter für die Größe(Höhe) des Mitglieds
     *
     * @return hoehe als float Wert.
     */
    public float getHoehe() {
        return hoehe;
    }

    /**
     * Setter für die Größe(Höhe) des Mitglieds. Die Größe muss zwischen 1 und 3 Meter liegen.
     *
     * @param hoehe nimmt den Höhenwert auf.
     */
    public void setHoehe(float hoehe) {
        try{
            if((hoehe < 1.0) || (hoehe > 3.0)){
                System.out.println("Höhe muss zwischen 1 und 3 Metern liegen!");
            }else {
                this.hoehe = hoehe;
            }
        }
        catch (NumberFormatException e){
            System.out.println("Undefinierter Input: "+ e);
        }
    }

    /**
     * Getter für das gewählte Packet.
     *
     * @return gewaehlesPacket Wert als String.
     */
    public String getGewaehltesPacket() {
        return gewaehlesPacket;
    }

    /**
     * Setter für das gewählte Packet.
     *
     * @param gewaehlesPacket übernimmt den gewaehltsPacket Wert.
     */
    public void setGewaehltesPacket(String gewaehlesPacket) {
        this.gewaehlesPacket = gewaehlesPacket;
    }

    /**
     * Getter für die Bewertung.
     *
     * @return bewertung als HashMap Wert.
     */
    public HashMap getBewertung() {
        return bewertung;
    }

    /**
     * toString-Methode, die die Details eines Mitglieds als formatierten String zurückgibt.
     *
     * @return formatierter String für die Mitgliederdetails.
     */
    public String toString(){
        String str = super.toString();
        str += "Höhe: " + hoehe + " m\n";
        str += "Start Gewicht: " + startGewicht + " kg\n";
        str += "Gewähltes Packet: " + gewaehlesPacket +"\n";
        return str;
    }
}
