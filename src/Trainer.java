/**
 * Die Trainer-Klasse ist eine Subklasse von Person und
 * wird von Trainern beim Zugriff auf die Anwendung verwendet.
 *
 * @author Clemens Kerber
 * @version 1.0 (16.05.2021)
 */

public class Trainer extends Person{
    private String spezialitaet;

    /**
     * Constructor von Trainer
     * @param email EMail des Trainers
     * @param name Name des Trainers
     * @param address Adresse des Trainers
     * @param geschlecht Geschlecht des Trainers
     * @param spezialitaet Spezialität (Gebiet) des Trainers
     */
    public Trainer(String email, String name, String address, String geschlecht, String spezialitaet) {
        super(email, name, address, geschlecht);
        setSpezialitaet(spezialitaet);
    }

    /**
     * Getter für die Spezialität (Fähigkeit).
     *
     * @return spezialitaet des Trainers
     */
    public String getSpezialitaet() {
        return spezialitaet;
    }

    /**
     * Setter für die Spezialität.
     * @param spezialitaet Nimmt die neue Spezialität des Trainers auf
     */
    public void setSpezialitaet(String spezialitaet) {
        this.spezialitaet = spezialitaet;
    }

    /**
     * toString-Methode zur Rückgabe von Trainerdetails in einem formatierten String.
     *
     * @return formatierter String über die Infos des Trainers.
     */
    public String toString(){
        String str = super.toString();
        str += "Spezialität: " + spezialitaet + "\n";
        return str;
    }
}