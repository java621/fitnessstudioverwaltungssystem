/**
 * In der Bewertungsklasse werden Details aus einer bestimmten Bewertung für ein bestimmtes Mitglied im Fitnessstudio gespeichert.
 * Jede Bewertung enthält eine Aufzeichnung von Gewicht, Oberschenkel, Taille und einem Bewertungskommentar eines Mitglieds.
 *
 * @author Clemens Kerber
 * @version 1.0 (20.05.2021)
 */

public class Bewertung {
    private float gewicht;
    private float oberSchenkel;
    private float taille;
    private String kommentar;

    /**
     * Constructor Klasse-Bewertung.
     *
     * @param gewicht Aufzeichnung der Gewichtsmessung des Mitglieds
     * @param oberSchenkel Aufzeichnung der Oberschenkelmessung des Mitglieds
     * @param taille Aufzeichnung des Taillenumfangs des Mitglieds
     * @param kommentar Kommentar eines Trainers zum Bewerten
     */
    public Bewertung(float gewicht, float oberSchenkel, float taille, String kommentar) {
        this.gewicht = gewicht;
        this.oberSchenkel = oberSchenkel;
        this.taille = taille;
        this.kommentar = kommentar;
    }

    /**
     * Getter Gewicht
     *
     * @return gewicht
     */
    public float getGewicht() {
        return gewicht;
    }

    /**
     * Setter für Gewicht
     *
     * @param gewicht Wert
     */
    public void setGewicht(float gewicht) {
        this.gewicht = gewicht;
    }

    /**
     * Getter Oberschenkelumfang
     *
     * @return oberSchenkel
     */
    public float getOberschenkel() {
        return oberSchenkel;
    }

    /**
     * Setter für Oberschenkelumfang
     *
     * @param schenkel Wert
     */
    public void setSchenkel(float schenkel) {
        this.oberSchenkel = schenkel;
    }

    /**
     * Getter für Tailleumfang.
     *
     * @return taille
     */
    public float getTaille() {
        return taille;
    }

    /**
     * Setter für Taillenumfang
     *
     * @param taille Wert
     */
    public void setTaille(float taille) {
        this.taille = taille;
    }

    /**
     * Getter für Kommentar
     *
     * @return kommentar
     */
    public String getKommentar() {
        return kommentar;
    }

    /**
     * Setter für Kommentar
     * @param kommentar Wert
     */
    public void setKommentar(String kommentar) {
        this.kommentar = kommentar;
    }

    /**
     * toString-Methode zur Anzeige der Bewertungen im String-Format durch Ausgabe jedes Feldes in einem formatierten String.
     *
     * @return String mit allen Feldern der Bewertung.
     */
    @Override
    public String toString() {
        String str = "";
        str += "Gewicht: " + gewicht + "\n";
        str += "Taille: " + gewicht + "\n";
        str += "Oberschenkel: " + gewicht + "\n";
        str += "Kommentar: " + gewicht + "\n";
        return str;
    }
}
