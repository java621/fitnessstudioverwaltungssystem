/**
 * Die Person-Klasse ist die Superklasse aller Nutzer im Fitnessstudio. Hier werden gemeinsame Felder aller Nutzer des Fitnessstudios gespeichert:
 *
 * @author Clemens Kerber
 * @version 1.0 (15.05.2021)
 */

public class Person {
    private String name;
    private String email;
    private String address;
    private String geschlecht;

    /**
     * Constructor für die Klasse Person
     *
     * @param name Feld für den Namen einer Person
     * @param email Feld für die E-Mail-Adresse einer Person
     * @param address Feld für die Adresse einer Person
     * @param geschlecht Feld für das Geschlecht einer Person
     */
    public Person(String name, String email, String address, String geschlecht) {
        setName(name);
        this.email = email;
        this.address = address;
        setGeschlecht(geschlecht);
    }

    /**
     *Getter für den Namen einer Person
     * @return name Person
     */
    public String getName() {
        return name;
    }

    /**
     * Setter für den Namen. Maximal 30 Zeichen lang.
     * @param name Der Name, der dem Person Objekt übergeben werden soll.
     */
    public void setName(String name) {
        if (name.length() > 30) {
            this.name = name.substring(0, 30);
        } else {
            this.name = name;
        }
    }

    /**
     * Getter für die EMail einer Person
     * @return email Person
     */
    public String getEmail() {
        return email;
    }

    /**
     * Getter für die Adresse einer Person
     * @return addresse Person
     */
    public String getAddress() {
        return address;
    }

    /**
     * Setter für die Adresse.
     * @param address nimmt den Wert an, wenn die Adresse aktualisiert wird.
     */
    public void setAddress(String address) {
        this.address = address;
    }

    /**
     * Getter für das Geschlecht einer Person
     * @return geschlecht Person
     */
    public String getGeschlecht() {
        return geschlecht;
    }

    /**
     * Setter für das Feld Geschlecht. Wenn der übergebene Wert nicht 'M' oder 'F' ist, wird das Geschlecht auf Undefiniert gesetzt.
     * @param geschlecht nimmt den Wert an, auf den das Geschlecht aktualisiert werden soll.
     */
    public void setGeschlecht(String geschlecht) {
        if ((geschlecht.equals("M") || (geschlecht.equals("F")))) {
            this.geschlecht = geschlecht;
        } else {
            this.geschlecht = "Undefiniert!";
        }
    }

    /**
     * Eine toString-Methode die die Informationen über eine Person liefert.
     * @return Ein String, der die Details der Person enthält.
     */
    public  String toString(){
        String str = "";
        str += "Name: " + name + "\n";
        str += "Email: " + email + "\n";
        str += "Adresse: "+ address + "\n";
        str += "Geschlecht: " + geschlecht + "\n";
        return str;
    }
}
