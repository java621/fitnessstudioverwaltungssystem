/**
 * Die Klasse StudentMitglied ist eine Subklasse von Mitglied und soll von Studenten verwendet werden.
 *
 * @author Clemens Kerber
 * @version 1.0(16.05.2021)
 */
public class StudentMitglied extends Mitglied{
    private int studentenId;
    private String collegName;

    /**
     * Constructor von der Klasse StudentMitglied.
     *
     * @param name Name des Mitglieds
     * @param email E-Mail des Mitglieds
     * @param address Adresse des Mitglieds
     * @param geschlecht Geschlecht des Mitglieds
     * @param hoehe Größe (Höhe) des Mitglieds
     * @param startGewicht Startgewicht des Mitglieds
     * @param gewaehlesPacket Gewähltes Packet des Mitglieds
     * @param studentenId Studenten ID des Mitglieds
     * @param collegName Hochschule (UNI) des Mitglieds
     */
    public StudentMitglied(String name, String email, String address, String geschlecht, float hoehe, float startGewicht, String gewaehlesPacket, int studentenId, String collegName) {
        super(name, email, address, geschlecht, hoehe, startGewicht, gewaehlesPacket);
        this.studentenId = studentenId;
        this.collegName = collegName;
    }

    /**
     * Methode, die das vom Studentenmitglieds gewählte Paket festlegt
     *
     * @param gewaehltesPacket gewähltes Packet
     */
    public void gewaehltesPacket(String gewaehltesPacket){

        setGewaehltesPacket(gewaehltesPacket);
    }

    /**
     * Getter für die Studenten-ID
     *
     * @return studentenId Wert
     */
    public int getStudentenId() {
        return studentenId;
    }

    /**
     * Getter für den Hochschul (UNI) Namen.
     *
     * @return colleg Wert
     */
    public String getCollegName() {
        return collegName;
    }

    /**
     * toString-Methode, die die Details eines Studenten Mitglieds als formatierten String zurückgibt.
     *
     * @return foramtierter String über die Infos vom Studenten Mitglieds
     */
    public String toString(){
        String str = super.toString();
        str += "Student ID: " + studentenId + "\n";
        str += "College Name: "+ collegName + "\n";
        return str;
    }
}