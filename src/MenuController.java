import java.util.*;
import java.text.SimpleDateFormat;

/**
 * Die MenuController-Klasse wird verwendet, um Ein- / Ausgänge zu verarbeiten und dem Benutzer Menüs anzuzeigen.
 * Im Wesentlichen ist der MenuController der „Treiber“ der Anwendung, der alle Klassen und Methoden zusammenführt.
 *
 * @author Clemens Kerber
 * @version 1.0
 */
public class MenuController {

    private final Scanner input;
    private final FitnessAPI gym;
    private Mitglied mitglieder;
    private StudentMitglied studentMitglied;
    private Trainer trainer;
    private final LinkedHashMap<String, String> packete;
    private final ArrayList<Mitglied> mitgliederliste;
    SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yy");

    /**
     * main-Methode, die den MenuController-Konstruktor aufruft und ausführt
     *
     * @param args
     */
    public static void main(String[] args) {
        MenuController app = new MenuController();
        app.run();
    }

    /**
     * Konstruktor für die Klasse MenuController.
     * Erstellt Instanzen von Scanner-, GymAPI- und LinkedHashMap-Klassen.
     */
    public MenuController() {
        input = new Scanner(System.in);
        gym = new FitnessAPI();
        packete = new LinkedHashMap<String, String>();
        mitgliederliste = gym.getMitglieder();
    }

    /**
     * Methode, die aufgerufen wird, um das Programm auszuführen.
     * Enthählt Aufrufe vieler Methoden aus der MenuController-Klasse und Methoden
     * aus externen Klassen.
     *
     */
    private void run() {
        //Methode zum Laden aus einer XML-Datei
        loadXML();

        //Methode, die Pakete zum Paketfeld hinzufügt
        packeteHinzufuegen();

        //Die Methode registrierungsStatus wird der Variable istRegistriert
        //zugewiesen, die überprüft, ob der Benutzer bereits registiert ist oder ein neuer Benutzer ist.
        boolean istRegistriert = registrierungsStatus();
        //Die benutzertyp-Methode wird der userType-Variable zugewiesen, die überprüft, ob der Benutzer ein Mitglied oder ein Trainer ist
        String benutzertyp = benutzertyp();

        //Die if else-Anweisung leitet den Benutzer zum richtigen Menü, je nachdem, ob er registriert
        //ist oder nicht, und ob er Mitglied oder Trainer ist.
        if (istRegistriert) {
            login(benutzertyp);
        } else {
            register(benutzertyp);
        }

        if (benutzertyp == "Trainer") {
            int option = trainerMenue();
            //Solange der Benutzer keine 0 eingibt, rufen Sie die trainerMenu-Methode weiter auf
            while (option != 0) {
                //switch-Anweisung zur Verarbeitung der Benutzereingaben
                switch (option) {
                    case 1:
                        mitgliederHinzufuegen();
                        break;
                    case 2:
                        System.out.println(gym.listMitglieder());
                        break;
                    case 3:
                        System.out.println("Geben Sie die E-Mail-Adresse des Mitglieds ein: ");
                        input.nextLine(); //scanner bug
                        String mitgliedEmail = input.nextLine();
                        System.out.println("");
                        Mitglied sucheMitglied = gym.sucheMitgliederMitEmail(mitgliedEmail);
                        //Wenn ein Mitglied gefunden wurde, gib die Mitgliedsdetails mit der toString-Methode wieder
                        if (sucheMitglied != null) {
                            System.out.println(sucheMitglied.toString());
                        } else {
                            System.out.println("Keine Mitglieder gefunden");
                        }
                        break;
                    case 4:
                        int unteroption = trainerUntermenue();
                        //Solange der Benutzer keine 0 eingibt, rufe weiterhin die TrainerUntermenue-Methode auf
                        while (unteroption != 0) {
                            //switch-Anweisung zur Verarbeitung der Benutzereingaben
                            switch (unteroption) {
                                case 1: //Hinzufügen einer neuen Bewertung
                                    //Bewertungsfelder initialisieren
                                    String akzeptierteMitgliederMail = "";
                                    float akzeptiertesGewicht = 0.00f;
                                    float akzeptierterOberschenkel = 0.00f;
                                    float akzeptierteTaille = 0.00f;
                                    String akzeptiertesKommentar = "";

                                    boolean guteEingabe = false;
                                    //Fahren Sie nur fort, wenn guteEingabe wahr ist, das bedeutet wenn ein Mitglied mit der eingegebenen E-Mail-Adresse existiert
                                    while (!guteEingabe) {
                                        try {
                                            System.out.println("Geben Sie die E-Mail-Adresse des Mitglieds ein:");
                                            akzeptierteMitgliederMail = input.nextLine();
                                            Mitglied mitglieder = gym.sucheMitgliederMitEmail(akzeptierteMitgliederMail);
                                            if (mitglieder == null) {
                                                System.out.println("Dies ist keine gültige Mitglieds-E-Mail - bitte geben Sie eine andere ein");
                                            } else {
                                                guteEingabe = true;
                                            }
                                        } catch (Exception e) {
                                            System.out.println("Fehler bei der Suche nach Mitglied per E-Mail: " + e);
                                        }
                                    }
                                    Date aktuellesDatum = new Date();
                                    //Die Zuweisung des aktuellen Datums zur Datumsvariablen 'formatter'
                                    //ermöglicht die Formatierung des Datums in TT/MM/JJ
                                    String datum = formatter.format(aktuellesDatum);

                                    //Nur fortfahren, wenn gutesGewicht wahr ist
                                    boolean gutesGewicht = false;
                                    while (!gutesGewicht){
                                        System.out.println("Gewicht (kg): ");
                                        try{
                                            akzeptiertesGewicht = input.nextFloat();
                                            if((akzeptiertesGewicht < 35.0) || (akzeptiertesGewicht > 250.0)){
                                                System.out.println("Das Startgewicht muss zwischen 35kg und 250kg liegen.");
                                            }else{
                                                gutesGewicht = true;
                                            }
                                        }
                                        catch (Exception e){
                                            System.out.println("Fehler beim Einstellen des Gewichtswerts: "+ e);
                                            input.nextLine();
                                        }
                                    }

                                    //Fahre fort, wenn guterOberschenkel wahr ist
                                    boolean guterOberschenkel = false;
                                    while (!guterOberschenkel){
                                        try{
                                            System.out.println("Oberschenkelumfang (cm): ");
                                            akzeptierterOberschenkel = input.nextFloat();
                                        guterOberschenkel = true;
                                        }
                                        catch (Exception e){
                                            System.out.println("Fehler beim Einstellen des Oberschenkelwertes: "+ e);
                                        }
                                    }

                                    //Fahre fort, wenn guteTaille wahr ist
                                    boolean guteTaille = false;
                                    while (!guteTaille) {
                                        try {
                                            System.out.println("Schenkel (cm): ");
                                            akzeptierteTaille = input.nextFloat();
                                            guteTaille = true;
                                        } catch (Exception e) {
                                            System.out.println("Fehler beim Einstellen des Oberschenkelwertes: " + e);
                                        }
                                    }

                                    //Fahre fort, wenn okKommentar wahr ist
                                    boolean okKommentar = false;
                                    while (!okKommentar) {
                                        try {
                                            System.out.println("Kommentar: ");
                                            akzeptiertesKommentar = input.nextLine();
                                            if (akzeptiertesKommentar == "") {
                                                System.out.println("Ihr Kommentar muss etwas enthalten!");
                                            } else {
                                                okKommentar = true;
                                            }
                                        } catch (Exception e) {
                                            System.out.println("Fehler bei der Kommentareingabe: " + e);
                                        }
                                    }

                                    //Neues Bewertungs-Objekt aus den eingegebenen Daten erstellen.
                                    Bewertung bewertung = new Bewertung(akzeptiertesGewicht, akzeptierterOberschenkel, akzeptierteTaille, akzeptiertesKommentar);
                                    //Suche das Mitglied aus der angegebenen E-Mail.
                                    Mitglied akzeptiertesMitglied = gym.sucheMitgliederMitEmail(akzeptierteMitgliederMail);
                                    //Bewertung zur Bewertung des Mitglieds hinzufügen HashMap
                                    akzeptiertesMitglied.bewertung.put(datum, bewertung);
                                    System.out.println("Bewertung erfolgreich hinzugefügt");
                                    System.out.println("");
                                    break;
                                case 2: //Aktualisieren eines Kommentars zu einer Bewertung
                                    Mitglied bewertetesMitglied = null;
                                    boolean guteEmail = false;
                                    while (!guteEmail) {
                                        System.out.println("Geben Sie die E-Mail-Adresse des Mitglieds ein:");
                                        bewertetesMitglied = gym.sucheMitgliederMitEmail(input.nextLine());
                                        if (bewertetesMitglied != null) {
                                            guteEmail = true;
                                        } else {
                                            System.out.println("E-Mail ungültig - Geben Sie eine andere E-Mail ein.");
                                        }
                                    }
                                    //Prüfen, ob das Mitglied bereits Bewertungen hat
                                    if (bewertetesMitglied.getBewertung().size() == 0) {
                                        System.out.println("Dieses Mitglied hat noch keine Bewertungen aufgezeichnet");
                                        System.out.println("");
                                    } else {
                                        HashMap mitgliederbewertung = bewertetesMitglied.getBewertung();
                                        SortedSet<String> sortiertDatum = bewertetesMitglied.sortierteBewertungDatum();
                                        String akzeptiertesDatum = "";
                                        String aktualiesiertesKommentar = "";
                                        //Drucke sortierte Termine aus, damit der Trainer entscheiden kann, zu welcher Bewertung er einen Kommentar aktualisieren möchte
                                        System.out.println(sortiertDatum);
                                        boolean gutesDatum = false;
                                        while (!gutesDatum) {
                                            System.out.println("Geben Sie das Datum der Bewertung ein, die Sie aktualisieren möchten (TT / MM / JJ):");
                                            akzeptiertesDatum = input.nextLine();
                                            //Stelle sicher, dass das eingegebene Datum mit einer vorhandenen Bewertung übereinstimmt
                                            if (mitgliederbewertung.get(akzeptiertesDatum) != null) {
                                                gutesDatum = true;
                                            } else {
                                                System.out.println("Ungültiges Datum - Geben Sie ein vorhandenes Bewertungsdatum ein");
                                            }
                                        }
                                        //Hole die ausgewählte Bewertung mit dem angegebenen Datum
                                        Bewertung gewaehlteBewertung = bewertetesMitglied.bewertung.get(akzeptiertesDatum);
                                        boolean gutesKommentar = false;
                                        //Stelle sicher, dass der Kommentar etwas enthält
                                        while (!gutesKommentar) {
                                            System.out.println("Geben Sie einen neuen Kommentar zur Bewertung ein: ");
                                            aktualiesiertesKommentar = input.nextLine();
                                            if (aktualiesiertesKommentar == "") {
                                                System.out.println("Ihr Kommentar muss etwas enthalten!");
                                            } else {
                                                gutesKommentar = true;
                                            }
                                        }
                                        try {
                                            //Aktualisiere den Kommentar in der ausgewählten Bewertung auf den neu eingegebenen Kommentar
                                            gewaehlteBewertung.setKommentar(aktualiesiertesKommentar);
                                            //Setze die gewählte Bewertung zurück in die mitgliederbewertung HashMap
                                            mitgliederbewertung.put(akzeptiertesDatum, gewaehlteBewertung);
                                            System.out.println("Kommentar erfolgreich aktualisiert");
                                            System.out.println("");
                                        } catch (Exception e) {
                                            System.out.println("Fehler beim Aktualisieren des Kommentars: " + e);
                                            System.out.println("");
                                        }
                                    }
                                    break;
                                default:
                                    System.out.println("Ungültige Option ausgewählt");
                            }
                            unteroption = trainerUntermenue();
                        }
                }
                System.out.println("");
                option = trainerMenue();
            }
        } else if (benutzertyp == "Mitglied") {
            int option = mitgliederMenue(); //Mitgliedermenü anzeigen
            //Solange der Benutzer keine 0 eingibt, rufe weiterhin die Methode mitgliederMenue auf
            while (option != 0) {
                switch (option) {
                    case 1:
                        //Die Details des Mitglieds als String ausgeben
                        System.out.println(mitglieder.toString());
                        break;
                    case 2: //Aktualisieren der Mitgliedsdaten
                        try {
                            System.out.println("Bitte geben Sie Ihren Namen ein:");
                            input.nextLine();
                            mitglieder.setName(input.nextLine());
                            System.out.println("Bitte geben Sie Ihr Geschlecht ein ('M' oder 'F'):");
                            mitglieder.setGeschlecht(input.nextLine().toUpperCase());
                            System.out.println("Bitte geben Sie Ihre Größe (m) ein:");
                            mitglieder.setHoehe(input.nextFloat());
                            System.out.println("Bitte geben Sie Ihr Gewicht (kg) ein:");
                            mitglieder.setStartGewicht(input.nextFloat());
                            mitglieder.setGewaehltesPacket(paketAnzeigen(packete));
                            System.out.println("");
                            System.out.println("Details erfolgreich aktualisiert");
                        } catch (Exception e) {
                            System.out.println("Fehler beim Aktualisieren der Details: " + e);
                        }
                        break;
                    case 3:
                        int wahl = mitgliederUntermenue(); //Untermenü für Mitglieder anzeigen
                        while (wahl != 0) {
                            switch (wahl) {
                                case 1: //Bewertungen nach Gewicht geordnet anzeigen
                                    System.out.println(mitglieder.getBewertungMitGewicht());
                                    break;
                                case 2: //Bewertungen nach Taillenumfang geordnet anzeigen
                                    System.out.println(mitglieder.getBewertungMitTaille());
                                    break;
                            }
                            wahl = mitgliederMenue();
                        }
                        System.out.println("");
                        break;
                }
                System.out.println("");
                option = mitgliederMenue();
            }
        }
        saveXML(); //Beim Beenden der Anwendung in XML-Datei speichern

    }

    /**
     * Methode, mit der überprüft wird, ob der Benutzer bereits
     * registriert ist oder ob er ein neues Konto erstellen muss
     * @return ob der Benutzer bereits registriert ist oder nicht
     */
    private  boolean registrierungsStatus(){
        boolean ergebnis = false;
        boolean guterWert = false;

        while (!guterWert){
            try {
                System.out.println("Wilkommen in der Fintessstudio App");
                System.out.println("Drücke 'l' für login");
                System.out.println("Drücke 'r' für registrieren");
                System.out.println("===> ");
                char login = input.nextLine().charAt(0);
                if(login == 'l'){
                    ergebnis = true;
                    guterWert = true;
                }else if(login == 'r'){
                    ergebnis = false;
                    guterWert = true;
                }
            }
            catch (Exception e){
                input.nextLine();
                System.out.println("Zeichen 'l' (login) oder 'a' (Anmelden) erwartet.");
            }
        }
        return ergebnis;
    }

    /**
     * Methode zur Feststellung, ob der Benutzer ein Mitglied oder ein Trainer ist
     *
     * @return String Mitglied oder Trainer
     */
    private String benutzertyp(){
        String benutzertyp = "";
        boolean guterWert = false;

        while (!guterWert){
            try {
                System.out.println("Geben Sie 'm' ein, wenn Sie Mitglied sind");
                System.out.println("Geben Sie 't' ein, wenn Sie Trainer sind");
                char typ = input.nextLine().charAt(0);
                if(typ == 'm'){
                    benutzertyp = "Mitglied";
                    guterWert = true;
                }else if (typ == 't'){
                    benutzertyp = "Trainer";
                    guterWert = true;
                }else{
                    System.out.println("Zeichen 'm' (Mitglied) oder 't' (Trainer) erwartet.");
                }
            }
            catch (Exception e){
                input.nextLine();
                System.out.println("Zeichen 'm' (Mitglied) oder 't' (Trainer) erwartet.");
            }
        }
        return benutzertyp;
    }

    /**
     * Methode zum Anmelden eines Mitglieds oder Trainers
     * @param beutzertyp Mitglied oder Trainer als String
     */
    private void login(String beutzertyp){
        System.out.println("Bitte geben Sie Ihre Email Adresse ein: ");
        String email = input.nextLine();

        if(beutzertyp == "Mitglied"){
            Mitglied ergebnis = gym.sucheMitgliederMitEmail(email);
            if(ergebnis != null){
                mitglieder = ergebnis;
            }else {
                System.out.println("Zugriff abgelehnt");
                System.exit(0);
            }
        }else if(beutzertyp == "Trainer"){
            Trainer ergebnis = gym.sucheTrainerMitEmail(email);
            if(ergebnis != null){
                trainer = ergebnis;
            }else{
                System.out.println("Zugriff abgelehnt");
                System.exit(0);
            }
        }
        System.out.println("Anmeldung erfolgreich");
        System.out.println("");
    }

    /**
     * Methode zum Registrieren eines neuen Benutzers
     * @param benutzertyp Mitglied oder Trainer als String
     */
    private void register(String benutzertyp){

        System.out.println("Name eingeben: ");
        String name = "";
        boolean guterName = false;

        while (!guterName){
            try {
                name = input.nextLine();
                guterName = true;
            }
            catch (Exception e){
                System.out.println("Unerwartete E-Mail-Eingabe - String erwartet.");
            }
        }

        // emailEintrag-Methode, die aufgerufen wird, um die E-Mail-Adresse des Benutzers zu erhalten
        String benutzerEmail = emailEintrag();

        System.out.println("Adresse eingeben: ");
        String adresse = "";
        boolean guteAdresse = false;

        while (!guteAdresse){
            try {
                adresse = input.nextLine();
                guteAdresse = true;
            }
            catch (Exception e){
                System.out.println("Unerwartete Adresse-Eingabe - String erwartet.");
            }
        }
        System.out.println("Geschlecht eingeben: ('M' oder 'F')");
        String geschlecht = "";
        boolean gutesGeschlecht = false;

        while (!gutesGeschlecht){
            try {
                geschlecht = input.nextLine().toUpperCase();
                gutesGeschlecht = true;
            }
            catch (Exception e){
                System.out.println("Unerwartete Geschlechts-Eingabe - String erwartet");
                input.nextLine();
            }
        }
        if(benutzertyp == "Mitglied"){
            float hoehe = 0.00f;
            boolean guteHoehe = false;
            while (!guteHoehe){
                System.out.println("Größe eingeben (m): ");
                try {
                    hoehe = input.nextFloat();
                    //Die eingegebene Höhe zwischen 1 und 3 Metern
                    if((hoehe < 1.0) || (hoehe > 3.0)){
                        System.out.println("Größe muss zwischen 1 und 3 Meter betragen.");
                    }else{
                        guteHoehe = true;
                    }
                }
                catch (Exception e){
                    System.out.println("Eingabe muss eine Zahl zwischen 1 und 3 sein.");
                    input.nextLine();
                }
            }
            float startGewicht = 0.00f;
            boolean gutesGewicht = false;
            while (!gutesGewicht){
                System.out.println("Startgewicht (kg) eingeben:");
                try {
                    startGewicht = input.nextFloat();
                    //Das eingegebene Gewicht zwischen 35 und 250 kg
                    if((startGewicht < 35.0) || (startGewicht > 250.0)){
                        System.out.println("Startgewicht muss zwischen 35kg und 250kg betragen.");
                    }else{
                        gutesGewicht = true;
                    }
                }
                catch (Exception e){
                    System.out.println("Eingabe muss eine Zahl zwischen 35 und 250 betragen.");
                    input.nextLine();
                }
            }
            boolean guteEingabe = false;
            while (!guteEingabe){
                try {
                    //Prüfen, ob der Benutzer ein Student ist
                    System.out.println("Sind sie ein Student? (J/N)");
                    input.nextLine();
                    char antwort = input.nextLine().charAt(0);
                    //Wenn der Benutzer mit 'J' antwortet, wird der Benutzer nach seiner studentID und seinem hochSchulNamen(UNI) gefragt
                    if(antwort == 'J'){
                        guteEingabe = true;
                        System.out.println("Bitte geben sie ihre Studenten ID ein: ");
                        int studentenID = input.nextInt();
                        System.out.println("Bitte geben sie den Namen der Hochschule ein:");
                        input.nextLine();
                        String hochschulName = input.nextLine();
                        //Der StudentMitglied-Konstruktor wird aufgerufen und die vom Benutzer eingegebenen Daten werden an den Konstruktor übergeben.
                        //Der College-Name des Benutzers wird als ausgewähltes Paket übergeben
                        gym.mitgliederHinzufuegen(new StudentMitglied(name,benutzerEmail,adresse,geschlecht,hoehe,startGewicht,hochschulName, studentenID,hochschulName));
                        mitglieder = gym.sucheMitgliederMitEmail(benutzerEmail);
                    }else if (antwort == 'N') { //Wenn der Benutzer kein Student ist, werden die Paketoptionen angezeigt
                        guteEingabe = true;
                        //Das Feld gewaehltesPacket enthält das Paket, das der Benutzer beim Anzeigen der Methode PaketAnzeigen auswählt
                        String gewaehltesPacket = paketAnzeigen(packete);
                        //Der Mitglieder-Konstruktor wird aufgerufen und die vom Benutzer eingegebenen Daten werden übergeben
                        gym.mitgliederHinzufuegen(new Mitglied(name, benutzerEmail, adresse, geschlecht, hoehe, startGewicht, gewaehltesPacket));
                        //Das Mitgliedsfeld wird nach Abschluss der Registrierung auf das neue Mitglied gesetzt
                        mitglieder = gym.sucheMitgliederMitEmail(benutzerEmail);
                    }else{
                        System.out.println("Zeichen 'J' oder 'N' erwartet, Sie haben etwas anderes eingegeben");
                        System.out.println("");
                    }
                }
                catch (Exception e){
                    System.out.println("Registrierungsfehler aufgetreten: +"+e);
                }
            }
        }else if (benutzertyp == "Trainer"){
            //Wenn der Benutzer, der sich registriert, ein Trainer ist, wird er nach seiner Spezialität gefragt
            System.out.println("Bitte geben Sie Ihre Spezialität ein:");
            //Der Trainerkonstruktor wird dann aufgerufen
            String spezialitaet = input.nextLine();
            gym.trainerHinzufuegen(new Trainer(benutzerEmail, name, adresse, geschlecht, spezialitaet));
            //Der neu registrierte Trainer wird der Trainervariablen zugewiesen
            trainer = gym.sucheTrainerMitEmail(benutzerEmail);
        }
        System.out.println("Registrierung erfolgreich");
        System.out.println("");
    }

    /**
     * Methode zur Anzeige des Trainermenüs
     *
     * @return die vom Benutzer als int gewählte Nummer
     */
    private int trainerMenue() {
        int option = 0;
        boolean guterWert = false;

        while (!guterWert) {
            try {
                //Das Trainer-Menü wird angezeigt
                System.out.println("Trainer Menü:");
                System.out.println("1. Neues Mitglied hinzufügen");
                System.out.println("2. Alle Mitglieder ausgeben");
                System.out.println("3. Suche Mitglied per Email");
                System.out.println("4. Bewertung");
                System.out.println("0. Exit");
                System.out.println("===> ");
                option = input.nextInt();
                if (option >= 0 && option <= 4) {
                    guterWert = true;
                } else {
                    System.out.println("Bitte geben sie eine Zahl zwischen 0 und 4 ein");
                }
            } catch (Exception e) {
                input.nextLine();
                System.out.println("Zahl erwartet - Sie haben etwas anderes eingegeben");
            }
        }
        return option;
    }

    /**
     * Methode zum Anzeigen des Mitgliedermenüs
     *
     * @return die vom Benutzer als int gewählte Nummer
     */
    private int mitgliederMenue(){
        int option = 0;
        boolean guterWert = false;

        while (!guterWert){
            try{ //Das Mitgliedermenü wird angezeigt
                System.out.println("Mitglied Menü:");
                System.out.println("1. Dein Profil anzeigen");
                System.out.println("2. Dein Pfrofil updaten");
                System.out.println("3. Fortschritt");
                System.out.println("0. Exit");
                System.out.println("===> ");
                option = input.nextInt();
                if(option >= 0 && option <= 3){
                    guterWert = true;
                }else{
                    System.out.println("Bitte geben sie eine Zahl zwischen 0 und 3 ein");
                }
            }
            catch (Exception e){
                input.nextLine();
                System.out.println("Zahl erwartet - Sie haben etwas anderes eingegeben");
            }
        }
        return option;
    }

    /**
     * Methode zur Anzeige des Trainer-Untermenüs
     *
     * @return die vom Benutzer als int gewählte Nummer
     */
    private int trainerUntermenue() {
        int option = 0;
        boolean guterWert = false;

        while (!guterWert) {
            try {
                System.out.println("Untermenü Bewertungen");
                System.out.println("1. Fügen sie eine Bewertung für ein Mitglied hinzu");
                System.out.println("2. Kommentar zu einer Bewertung für ein Mitglied aktualisieren");
                System.out.println("0. Zurück zum Trainermenü");
                System.out.println("===> ");
                option = input.nextInt();
                if (option >= 0 && option <= 2) {
                    guterWert = true;
                } else {
                    System.out.println("Bitte geben sie eine Zahl zwischen 0 und 2 ein");
                }
            } catch (Exception e) {
                System.out.println("Bitte geben sie eine Zahl zwischen 0 und 2 ein");
            }
        }
        return option;
    }

    /**
     * Methode zum Anzeigen des Mitgliedsuntermenüs
     *
     * @return die vom Benutzer als int gewählte Nummer
     */
    private  int mitgliederUntermenue(){
        int option = 0;
        boolean guterWert = false;

        try {
            while(!guterWert){ //Mitgliedsuntermenü wird angezeigt
                System.out.println("Untermenü Fortschritt");
                System.out.println("1. Fortschritt nach Gewicht anzeigen");
                System.out.println("2. Anzeigen des Fortschritts durch Taillenumfang ");
                System.out.println("0. Zurück zum Mitgliedermenü");
                System.out.println("===> ");
                option = input.nextInt();
                if(option >= 0 && option <= 2){
                    guterWert = true;
                }else{
                    System.out.println("Bitte geben sie eine Zahl zwischen 0 und 2 ein");
                }
            }
        }
        catch (Exception e){
            System.out.println("Bitte geben sie eine Zahl zwischen 0 und 2 ein");
        }
        return option;
    }

    /**
     * Methode, die verwendet wird, wenn ein neues Mitglied zum Fitnessstudio hinzugefügt wird
     */
    private void mitgliederHinzufuegen(){

        //Die Methode ist der Registrierung sehr ähnlich, außer dass sie anders angezeigt wird, da sie
        //verwendet werden soll, wenn ein Trainer ein neues Mitglied anmeldet
        String mitgliederEmail = emailEintrag();

        System.out.println("Mitgliedsname eingeben: ");
        String name = "";
        boolean guterName = false;
        while(!guterName){
            try {
                name = input.nextLine();
                guterName = true;
            }
            catch (Exception e){
                System.out.println("Unerwartete Eingabe: " + e);
            }
        }

        System.out.println("Mitgliedsadresse eingeben:");
        String adress = "";
        boolean guteAdresse = false;
        while (!guteAdresse){
            try {
                adress = input.nextLine();
                guteAdresse = true;
            }
            catch (Exception e){
                System.out.println("Unerwartete Eingabe: "+ e);
            }
        }

        System.out.println("Geben Sie das Geschlecht des Mitglieds ein ('M' oder 'F'):");
        String geschlecht = "";
        boolean gutesGeschlecht = false;
        while (!gutesGeschlecht){
            try {
                geschlecht = input.nextLine().toUpperCase();
                gutesGeschlecht = true;
            }
            catch (Exception e){
                System.out.println("Unerwartete Eingabe: " + e);
            }
        }

        Float hoehe = 0.00f;
        boolean guteHoehe = false;
        while (!guteHoehe){
            System.out.println("Mitglied Größe eingeben (m):");
            try {
                hoehe = input.nextFloat();
                if((hoehe < 1.0) || (hoehe > 3.0)){
                    System.out.println("Die Größe muss zwischen 1 und 3 Mether liegen.");
                }else{
                    guteHoehe = true;
                }
            }
            catch (Exception e){
                System.out.println("Die Eingabe muss eine Ganzzahl zwischen 1 und 3 sein.");
            }
        }

        Float startGewicht  = 0.00f;
        boolean gutesGewicht = false;
        while(!gutesGewicht){
            System.out.println("Mitglied Startgewicht eingeben (kg):");
            try{
                startGewicht = input.nextFloat();
                if((startGewicht < 35.0) || (startGewicht > 250.0)){
                    System.out.println("Startgewicht muss zwischen 35kg und 250kg liegen.");
                }else{
                    gutesGewicht = true;
                }
            }
            catch (Exception e){
                System.out.println("Eingabe muss eine Ganzzahl zwischen 35 und 250 sein");
            }
        }

        boolean guteEingabe = false;
        while (!guteEingabe){
            try {
            System.out.println("Ist dieses Mitglied ein Student? (J/N)");
            input.nextLine();
            char antwort = input.nextLine().toUpperCase().charAt(0);

                if (antwort == 'J') {
                    guteEingabe = true;
                    System.out.println("Bitte geben sie die neue Studentennummer des Mitglieds ein: ");
                    int studentenId = input.nextInt();
                    System.out.println("Bitte geben sie den Namen der Hochschule des neuen Mitglieds ein: ");
                    input.nextLine();
                    String hochschulName = input.nextLine();
                    gym.mitgliederHinzufuegen(new StudentMitglied(mitgliederEmail, name, adress, geschlecht, hoehe, startGewicht, hochschulName, studentenId, hochschulName));
                } else if (antwort == 'N') {
                    guteEingabe = true;
                    String gewaehltesPacket = paketAnzeigen(packete);
                    gym.mitgliederHinzufuegen(new Mitglied(mitgliederEmail, name, adress, geschlecht, hoehe, startGewicht, gewaehltesPacket));
                } else {
                    System.out.println("Buchstaben 'J' oder 'N' werden akzeptiert, sie haben etwas anderes eingegeben.");
                    System.out.println("");
                }
            }
            catch (Exception e){
                System.out.println("Registrierungsfehler aufgetreten: " + e);
            }
        }
        System.out.println("Mitglied erfolgreich registriert");


    }

    /**
     * Methode zum Hinzufügen von Packeten zu Packete HashMap
     */
    private void packeteHinzufuegen(){
        packete.put("Packet 1", "Erlaubt jederzeit den Zugang zum Fitnessstudio.\n Kostenloser Zugang zu allen Klassen. \n Zugang zu allen Umkleidekabinen, einschließlich Deluxe-Umkleidekabinen.");
        packete.put("Packet 2", "Erlaubt jederzeit den Zugang zum Fitnessstudio.\n 3 € Gebühr für alle Klassen. \n Zugang zu allen Umkleidekabinen, einschließlich Delux-Umkleidekabinen.");
        packete.put("Packet 3", "Erlaubt den Zugang zum Fintnessstudio außerhalb der Hauptverkehrszeiten. \n 5 € Gebühr für alle Klassen. \n Kein Zugang zu Deluxe-Umkleidekabinen");
        packete.put("WIT", "Erlaubt Zugang zum Fitnesstudio während der Semesterzeit. \n 4 € Gebühr für alle Klassen. \n Kein Zugang zu Deluxe-Umkleidekabinen." );
    }

    /**
     * Methode, die die Paketauswahl anzeigt und die Auswahl vom Benutzer zurückgibt
     *
     * @param packete nimmt die HashMap der anzuzeigenden Pakete auf
     * @return die Paketauswahl durch den Benutzer
     */
    private String paketAnzeigen(LinkedHashMap<String, String> packete){
        String ausgabe = "";
        int count = 1;

        //Iterator zum Durchlaufen der HashMap
        Iterator it = packete.entrySet().iterator();

        while (it.hasNext()){
            Map.Entry gymPacket = (Map.Entry)it.next();
            String gymPacketLeistungen = (String)gymPacket.getValue();
            String gymPacketName = (String) gymPacket.getKey();

            //Formatieren Sie eine Liste als String mit den Feldern gymPaketName und gymPacketLeistung
            ausgabe += count + ". " + gymPacketName + "\n" + gymPacketLeistungen + "\n\n";
            System.out.println("");
            count++;
        }
        System.out.println(ausgabe);

        boolean guteWerte = false;
        int wahl = 0;
        String packetWahl = "";

        while (!guteWerte){
            System.out.println("Bitte geben Sie Ihr gewünschtes Paket ein (1, 2, 3, 4):");
            try {
                wahl = input.nextInt();
                switch (wahl){ //Switch-Anweisung, die verwendet wird, um die Paketauswahl des Benutzers zu behandeln
                    case 1:
                        guteWerte = true;
                        packetWahl = "Packet 1";
                        break;
                    case 2:
                        guteWerte = true;
                        packetWahl = "Packet 2";
                        break;
                    case 3:
                        guteWerte = true;
                        packetWahl = "Packet 3";
                        break;
                    case 4:
                        guteWerte = true;
                        packetWahl = "WIT";
                        break;
                    default:
                        System.out.println("Unerwartete Eingabe - Nummer zwischen 1 und 4 erwartet.");
                        break;
                }
            }
            catch (Exception e){
                System.out.println("Zahl zwischen 1 und 4 erwartet.");
            }
        }
        return packetWahl;
    }

    /**
     * Methode zum Laden von Daten aus einer XML-Datei
     */
    private  void loadXML(){
        try {
            gym.load();
            System.out.println("Daten erfolgreich geladen");
        }
        catch (Exception e){
            System.err.println("Fehler beim Laden aus der Datei:" + e);
        }
    }

    /**
     *Methode zum Speichern von Daten in einer XML-Datei
     */
    private void saveXML(){
        try {
            gym.store();
            System.out.println("Daten erfolgreich geladen");
        }
        catch (Exception e){
            System.err.println("Fehler beim Laden aus der Datei:" + e);
        }
    }

    /**
     * Methode zur Validierung der E-Mail-Eingabe
     * @return String email
     */
    private String emailEintrag(){
        String email = "";
        boolean guteEmail = false;
        while (!guteEmail){
            try {
                boolean emailValidierung = false;
                while (!emailValidierung){
                    System.out.println("Enter Email: ");
                    email = input.nextLine();
                    if(email.contains("@") && (email.contains("."))){
                        emailValidierung = true;
                    }else {
                        System.out.println("Dies ist keine gültige E-Mail-Adresse - bitte versuchen Sie es mit einer anderen");
                        System.out.println("");
                    }
                }
                Mitglied ergebnis = gym.sucheMitgliederMitEmail(email);
                if(ergebnis != null){
                    System.out.println("Diese E-Mail wird bereits verwendet - bitte versuchen Sie es mit einer anderen");
                    System.out.println("");
                }else {
                    guteEmail = true;
                }
            }
            catch (Exception e){
                System.out.println("Unerwartete Eingabe: " + e);
            }
        }
        return email;
    }
}
