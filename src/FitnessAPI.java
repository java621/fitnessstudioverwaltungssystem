

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.DomDriver;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


/**
 * Die FitnessAPI-Klasse speichert die Listen der Mitglieder und Trainer im Fitnessstudio.
 * Die Klasse enthält auch verschiedene Methoden, die auf die gespeicherten Daten in
 * den Mitglieder- und Trainerlisten einwirken.
 *
 * @author Clemens Kerber
 * @version 1.0
 *
 */

    public class FitnessAPI {

        private ArrayList<Mitglied> mitglieder;
        private ArrayList<Trainer> trainer;

    /**
     * Konstruktor für die GymAPI-Klasse, die Mitglieder und Trainer neuen ArrayLists zuweist.
     */
    public  FitnessAPI() {
            this.mitglieder = new ArrayList<>();
            this.trainer = new ArrayList<>();
        }

    /**
     * Getter für die Mitglieder
     *
     * @return Mitglieder Wert ArrayList
     */
    public ArrayList<Mitglied> getMitglieder() {
            return mitglieder;
        }

    /**
     * Getter für die Trainer
     *
     * @return Trainer Wert ArrayList
     */
    public ArrayList<Trainer> getTrainer() {
            return trainer;
        }

    /**
     * Diese Methode fügt neue Mitglieder zur ArrayList mitglieder hinzu.
     * @param mit Nimmt den Mitgliederwert auf, der der Mitglieder-ArrayList hinzugefügt werden soll.
     */
    public void mitgliederHinzufuegen(Mitglied mit){
            mitglieder.add(mit);
        }

    /**
     * Diese Methode fügt neue Trainer zu der ArrayList trainer hinzu.
     * @param train Nimmt den Trainerwert auf, der der Trainer-ArrayList hinzugefügt werden soll.
     */
        public void trainerHinzufuegen(Trainer train){
            trainer.add(train);
        }

    /**
     * Methode zur Überprüfung der Größe der Mitglieder ArrayList.
     *
     * @return ein int Wert über die Größe der Mitglieder-ArrayList.
     */
    public int anzahlMitglieder(){
            return mitglieder.size();
        }

    /**
     * Methode zur Überprüfung der Größe der Trainer ArrayList.
     *
     * @return ein int Wert über die Größe der Trainer-ArrayList
     */
    public int anzahlTrainer(){
            return trainer.size();
        }

    /**
     * Methode, mit der überprüft wird, ob ein übergebener int-Wert
     * ein gültiger Index in der Mitglieder-ArrayList ist
     *
     * @param index Wert der gecheckt werden muss.
     * @return  boolean Wert der Sagt, ob ein Wert gültig ist oder nicht
     */
    public boolean istGueltigerMitgliederIndex(int index){
            if(index >= 0 && index <= getMitglieder().size()-1){
                return true;
            }else{
                return false;
            }
        }

    /**
     * Methode, mit der überprüft wird, ob ein übergebener int-Wert ein
     * gültiger Index in der ArrayList des Trainers ist
     *
     * @param index Wert der gecheckt werden muss.
     * @return boolean Wert der Sagt, ob ein Wert gültig ist oder nicht
     */
    public boolean istGueltigerTrainerIndex(int index){
            if(index >= 0 && index <= getTrainer().size()-1){
                return true;
            }else{
                return false;
            }
        }

    /**
     * Methode zur Überprüfung, ob ein Mitglied im Fitnessstudio mit der eingegebenen E-Mail-Adresse vorhanden ist.
     *
     * @param emailEingang email die gesucht wird
     * @return ein Mitglied-Objekt das zu der E-Mail passt.
     */
    public Mitglied sucheMitgliederMitEmail(String emailEingang){
            for(Mitglied mit : mitglieder){
                if(mit.getEmail().equals(emailEingang)){
                    return mit;
                }
            }
            return null;
        }

    /**
     * Methode zur Überprüfung, ob ein Trainer im Fitnessstudio mit der eingegebenen E-Mail-Adresse vorhanden ist.
     *
     * @param emailEintrag email die gesucht wird
     * @return ein Trainer-Objekt das zu der E-Mail passt.
     */
    public Trainer sucheTrainerMitEmail(String emailEintrag){
            for(Trainer train : trainer){
                if(train.getEmail().equals(emailEintrag)){
                    return train;
                }
            }
            return null;
        }

    /**
     * Methode zur Überprüfung, ob ein Mitglied mit dem mitgegebenen Namen existiert oder nicht.
     *
     * @param nameEintrag Name der gesucht wird.
     * @return ein Mitglied-Objekt wenn der mitgegebene Name gefunden wird.
     */
    public ArrayList<String> sucheMitgliedBeiNamen(String nameEintrag){
            ArrayList<String> result = new ArrayList<>();
            for(Mitglied mit : mitglieder){
                if(mit.getName().toLowerCase().contains(nameEintrag.toLowerCase())){
                    result.add(mit.getName());
                }
            }
            return  result;
        }

    /**
     * Methode zur Überprüfung, ob ein Mitglied mit den mitgegebenen Namen existiert oder nicht.
     *
     * @param nameEintrag Name der gesucht wird.
     * @return ein Trainer-Objekt wenn der mitgegebene Name gefunden wird.
     */
    public ArrayList<String> sucheTrainerBeiNamen(String nameEintrag) {
            ArrayList<String> result = new ArrayList<>();
            for (Trainer train : trainer) {
                if (train.getName().toLowerCase().contains(nameEintrag.toLowerCase())){
                    result.add(train.getName());
                }
            }
            return result;
        }

    /**
     * Mehtode, die eine Kopie von der ArrayList von Mitgliedern zurückgibt, die im Fitnessstudio registriert sind.
     *
     * @return Kopie ArrayList Mitglieder
     */
    public List<Mitglied> listMitglieder(){

        return List.copyOf(mitglieder);
        }

    /**
     * Methode, um alle im Fitnesstudio registrierten Mitglieder zu finden,
     * die in der BMI-Kategorie sind.
     *
     * @param bmiKategorie String Wert der überprüfenden BMI-Kategorie
     * @return eine ArrayList von Mitgliedern, deren BMI-Kategorie übereinstimmt
     */
    public ArrayList<Mitglied> listeMigliederMitBMIKategorie(String bmiKategorie){
        ArrayList<Mitglied> idealGewichtMitglied = new ArrayList<>();
        for (Mitglied m : mitglieder){
            Bewertung bewertung;
            HashMap<String, Bewertung> mitgliederBewertung = m.getBewertung();
            if(mitgliederBewertung.isEmpty()){
                bewertung = null;
            }else{
                bewertung = m.letzteBewertung();
            }
            double mitgliedBMI = FitnessNuetzlichkeit.kalkuliereBMI(m, bewertung);
            String mitgliedBMIKategorie = FitnessNuetzlichkeit.bestimmenDerBmiKategorie(mitgliedBMI);
            if(mitgliedBMIKategorie.contains(bmiKategorie.toUpperCase())){
                idealGewichtMitglied.add(m);
            }
        }
        return  idealGewichtMitglied;
        }

    /**
     * Methode zum Speichern der eingegebenen Daten in einer XML-Datei 'data.xml'.
     *
     * @throws Exception Gibt alle Fehler, die beim Versuch, Daten in der XML-Datei zu speichern, aufgetreten sind zurück.
     */
    public void store() throws Exception{
            XStream xstream = new XStream(new DomDriver());

            Class<?>[] classes = new Class[]{Mitglied.class, Trainer.class, HashMap.class, Bewertung.class, StudentMitglied.class};

            XStream.setupDefaultSecurity(xstream);
            xstream.allowTypes(classes);

            ObjectOutputStream out = xstream.createObjectOutputStream(new FileWriter("data.xml"));

            out.writeObject(mitglieder);
            out.writeObject(trainer);
            out.close();
        }

    /**
     * Methode zum Laden von Daten aus einer XML-Datei mit dem Namen 'data.xml'
     *
     * @throws Exception Gibt alle Fehler zurück, die beim Laden von Daten aus der XML-Datei aufgetreten sind.
     */
    public void load() throws  Exception{
            XStream xStream = new XStream(new DomDriver());

            Class<?>[] classes = new Class[] {Mitglied.class, Trainer.class, HashMap.class, Bewertung.class, StudentMitglied.class};

            XStream.setupDefaultSecurity(xStream);
            xStream.allowTypes(classes);

            ObjectInputStream aus = xStream.createObjectInputStream(new FileReader("data.xml"));

            mitglieder = (ArrayList<Mitglied>) aus.readObject();
            trainer = (ArrayList<Trainer>)  aus.readObject();
            aus.close();
        }
    }


